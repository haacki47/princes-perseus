

;(function(){

	var top, left, divContext,
			el, sectionCamera, sectionFolders,
			doc = document;

	sectionCamera = doc.getElementById('sectionCamera');
	sectionFolders = doc.getElementById('sectionFolders');


	el = doc.getElementById('cameraContext');
	divContext = doc.getElementById('contextMenu');


	el.addEventListener('contextmenu', function(e){
		e.preventDefault();

		left = e.pageX + "px";
		top = e.pageY + "px";

		console.log(e.target);
		// console.log(this);

		if(e.target === sectionCamera || e.target === sectionFolders || e.target === this){
			divContext.style.display = "block";
			divContext.style.top = top;
			divContext.style.left = left;
		}

	}, false);

	window.addEventListener('click', function(e){
		if(e.target) divContext.style.display = "none";
	}, false);

	window.addEventListener('keydown', function(e){
		if(e.keyCode === 27) divContext.style.display = "none";
	},false);

})();