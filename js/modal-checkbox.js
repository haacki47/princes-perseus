

;(function(){

	var checkboxEl = document.getElementsByClassName('my-bold-checkbox-active')[0];
	var myText = document.getElementsByClassName('bold-icon-checkbox')[0];
	
	if(checkboxEl && myText){
		if(checkboxEl.checked){
		myText.style.fontWeight = "bold";
	}else{
		myText.style.fontWeight = "400";
	}


	checkboxEl.addEventListener('click', function(e){
		if(e.target.checked){
			myText.style.fontWeight = "bold";
		}else{
			myText.style.fontWeight = "400";
		}
	}, false);

	}
	
})();