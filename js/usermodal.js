;(function(){
	var doc = document;
	var openModalTech = doc.getElementById('openModalTech');
	var modalCameraTech = doc.getElementById('modalCameraTech');
	var htmlStyle = doc.getElementsByTagName('html');
	var closeButtonContact = doc.getElementById('closeButtonContact');

	openModalTech.addEventListener('click', function(){
		modalCameraTech.style.display = "block";
		htmlStyle[0].style.overflow = "hidden";
	}, false);

	closeButtonContact.addEventListener('click', function(){
		modalCameraTech.style.display = "none";
		htmlStyle[0].style.overflow = "auto";
	}, false);

	window.addEventListener('click', function(e){
		if(e.target === modalCameraTech){
			modalCameraTech.style.display = "none";
			htmlStyle[0].style.overflow = "auto";
		}
	}, false);

	window.addEventListener('keydown', function(e){
		if(e.keyCode === 27){
			modalCameraTech.style.display = "none";
			htmlStyle[0].style.overflow = "auto";
		}
	}, false);





})();