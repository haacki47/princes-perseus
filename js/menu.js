




	function openCity(evt, tabname) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active-modal", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(tabname).style.display = "block";
    if(tabname === 'setting'){
        var currentElement = document.getElementsByClassName('active-two')[0];
        currentElement.className += " active-modal";
    }
    if(tabname === 'create'){
        var currentElement = document.getElementsByClassName('active-one')[0];
        currentElement.className += " active-modal";
    }
    if(tabname === 'instr'){
        var currentElement = document.getElementsByClassName('active-three')[0];
        currentElement.className += " active-modal";
    }
}

var doc = document;

doc.getElementsByClassName('block-one')[0].addEventListener('mouseover', function(){
	doc.getElementsByClassName('block-one')[0].children[0].style.backgroundPosition = "0px 0px";
	this.style.color = "white";
}, false);
doc.getElementsByClassName('block-one')[0].addEventListener('mouseout', function(){
	doc.getElementsByClassName('block-one')[0].children[0].style.backgroundPosition = "0px -87px";
	this.style.color = "#000";
}, false);

doc.getElementsByClassName('block-two')[0].addEventListener('mouseover', function(){
	doc.getElementsByClassName('block-two')[0].children[0].style.backgroundPosition = "0px -173px";
	this.style.color = "white";
}, false);

doc.getElementsByClassName('block-two')[0].addEventListener('mouseout', function(){
	doc.getElementsByClassName('block-two')[0].children[0].style.backgroundPosition = "0px -242px";
	this.style.color = "#000";
}, false);

doc.getElementsByClassName('block-three')[0].addEventListener('mouseover', function(){
	doc.getElementsByClassName('block-three')[0].children[0].style.backgroundPosition = "0px -297px";
	this.style.color = "white";
}, false);

doc.getElementsByClassName('block-three')[0].addEventListener('mouseout', function(){
	doc.getElementsByClassName('block-three')[0].children[0].style.backgroundPosition = "0px -345px";
	this.style.color = "#000";
}, false);
