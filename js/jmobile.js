jQuery(document).ready(function(){
	/*
		*CONTEXT MENU WITH PROJECT LINKS
	*/
	var contextMainMobileNav = $('#dropdown-menu-content'),
			contextMainMobileButton = $('#dropdown-menu-button-content'),
			globalScreenWidth, globalWindow,
			contextMainMobileClose = $('.close-btn-navbar');
	/*
		*CONTEXT MENU WITH CAMS
	*/
	var contextCamsMobileNav = $('.per-content__aside-menu'),
			contextCamsMobileButton = $('.mobile-menu-btn'),
			contextCamsMobileClose = $('.btn-close-window-menu');

			globalWindow = $(window);
			globalScreenWidth = globalWindow.width();
			

			if(contextMainMobileNav){

				globalWindow.on('resize', function(){
					 globalScreenWidth = globalWindow.width();
					 onResize();
				});


			function onResize() {

				if(globalScreenWidth <= 768){
	
				contextMainMobileButton.click(function(){
					contextMainMobileNav.fadeIn();
				});
	
				contextMainMobileClose.click(function(){
					contextMainMobileNav.fadeOut();
				});
	
				globalWindow.click(function(e){
					if(e.target === contextMainMobileNav[0]){
						contextMainMobileNav.fadeOut();
					}
				});
	
				$('#openModalTech').click(function(){
					contextMainMobileNav.fadeOut();
				});
			}else{
				contextMainMobileNav.off('click');
				contextMainMobileClose.off('click');
				globalWindow.off('click');
				$('#openModalTech').off('click');
			}

			if(contextCamsMobileNav){
			if(globalScreenWidth <= 768){
				contextCamsMobileButton.click(function(){
					contextCamsMobileNav.fadeIn();
				});
	
				contextCamsMobileClose.click(function(){
					contextCamsMobileNav.fadeOut();
				});
	
				globalWindow.click(function(e){
					if(e.target === contextCamsMobileNav[0]){
						contextCamsMobileNav.fadeOut();
					}
				});
			}
		}else{
			globalWindow.off('click');
			if(contextMainMobileNav.css('display') === 'none'){
				console.log("hello world");
			}
		}



		}

			onResize();

			};

			


});