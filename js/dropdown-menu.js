;(function(){

	var doc = document,
			dropdown = doc.getElementsByClassName('dropdown-button'),
			dropdownLvl = doc.getElementsByClassName('dropdown-button-level'),
			dropdownMenu, dropdownArrow, arrowEl,
			i, levelDrop, x;


			dropdownArrow = document.getElementsByClassName('per-content__aside-menu--navbar');
			arrowEl = dropdownArrow[0].children[0].getElementsByClassName('icon-arrow-menu')[0];


			arrowEl.addEventListener('click', function(){
				var currentMenuArrow;
			currentMenuArrow=this.parentNode.parentNode.children[0].getElementsByClassName('dropdown-menu')[0].style;
			if(currentMenuArrow.display === "none"){
				currentMenuArrow.display = "block";
				arrowEl.style.transform = "rotate(-30deg)";
			}else{
				arrowEl.style.transform = "rotate(0deg)";
				currentMenuArrow.display = "none";
				if(dropdownLvl[0].style.display === "block"){
					dropdownLvl[0].style.display = "none";
				}
			}

			}, false);

			dropdownMenu = doc.getElementsByClassName('dropdown-menu');

			for(i = 0; i < dropdownMenu.length; i++){
				dropdownMenu[i].style.display = "none";
			}

			for(i = 0; i < dropdown.length; i++){
				dropdown[i].addEventListener('click' ,function(){
					var myDrop = this.parentNode.getElementsByClassName('dropdown-menu');
					for(var j = 0; j < myDrop.length; j++){
						if(myDrop[j].style.display === "none"){
							myDrop[j].style.display = "block";
							var translateArrow = myDrop[j].parentNode.getElementsByClassName('icon-arrow-menu');
							translateArrow[0].style.transform = "rotate(-30deg)";
						}else{
							var translateArrow = myDrop[j].parentNode.getElementsByClassName('icon-arrow-menu');
							myDrop[j].style.display = "none";

							for(var x = 0; x < dropdownLvl.length; x++){
								var dropChildEl = dropdownLvl[x].parentNode;
								dropChildEl.getElementsByTagName('ul')[0].style.display = "none";
								dropChildEl.getElementsByClassName('icon-arrow-menu')[0].style.transform = "rotate(0deg)";
							}
							translateArrow[0].style.transform = "rotate(0deg)";
						}
					}
				}, false);
			}

			for(i = 0; i < dropdownLvl.length; i++){
					dropdownLvl[i].addEventListener('click', function(){
						var items;
						levelDrop = this.parentNode;
						items = levelDrop.getElementsByTagName('ul');
						if(items[0].style.display === "block"){
							items[0].style.display = "none";
							items[0].parentNode.getElementsByClassName('icon-arrow-menu')[0].style.transform = "rotate(0deg)";
						}else{
							items[0].style.display = "block";
							items[0].parentNode.getElementsByClassName('icon-arrow-menu')[0].style.transform = "rotate(-30deg)";

						}
					}, false);
				}

})();