$(document).ready(function(){

    $("#formValidate").validate({

       rules:{

            name:{
                required: true,
                minlength: 4,
                maxlength: 32,
            },

            emphoe:{
                required: true,
                minlength: 6,
                maxlength: 32,
                email: true,
            },
       },

       messages:{

            name:{
                required: "Пустое поле",
                minlength: "Неверный формат",
                maxlength: "Неверный формат",
            },

            emphoe:{
                required: "Пустое поле",
                email: "Эл. почта отсутствует",
                minlength: "Неверный формат",
                maxlength: "Неверный формат",
            },

       },
    });

});