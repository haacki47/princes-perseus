;(function(){

	var doc = document,
			modalCamera, btnModalTabCamera, openModalTech,
			modalTabs, btnStyleClose, btnCamera, clickEventEl, openMenuCameraTabs, htmlStyle,
			i;

	// Find the modal, and close button
	modalCamera = doc.getElementById('modal-camera');
	btnCamera = doc.getElementsByClassName('btn-modal-camera-close')[0];
	clickEventEl = doc.getElementsByClassName('video');

	openMenuCameraTabs = doc.getElementById('openMenuCameraTabs');
	btnModalTabCamera = doc.getElementById('openTabsCamera');
	modalTabs = doc.getElementsByClassName('modal-tabs-window')[0];
	btnStyleClose = doc.getElementsByClassName('btn-style-close')[0];
	htmlStyle = doc.getElementsByTagName('html');
	openModalTech = doc.getElementById('openModalTech');
	modalTechButton = doc.getElementById('modal-tech-button');
	closeButtonContact = doc.getElementById('closeButtonContact');


	closeButtonContact.addEventListener('click', function(){
		modalCameraTech.style.display = "none";
		htmlStyle[0].style.overflow = "auto";
	});

	// Select all video blocks and watch
	for(i = 0; i < clickEventEl.length; i++){
		clickEventEl[i].addEventListener('click', function(){
			modalCamera.style.display = "block";
			htmlStyle[0].style.overflow = "hidden";
		}, false);
	}

	openMenuCameraTabs.addEventListener('click', function(){
		modalTabs.style.display = "block";
		htmlStyle[0].style.overflow = "hidden";
	}, false);

	openModalTech.addEventListener('click', function(){
		modalCameraTech.style.display = "block";
		htmlStyle[0].style.overflow = "hidden";
	});

	btnStyleClose.addEventListener('click', function(){
		modalTabs.style.display = "none";
		htmlStyle[0].style.overflow = "auto";
	}, false);



	btnCamera.addEventListener('click', function(){
		modalCamera.style.display = "none";
		htmlStyle[0].style.overflow = "auto";
	}, false);

	window.addEventListener('click', function(e){
		if(e.target === modalCamera || e.target === modalTabs || e.target === modalCameraTech){
			modalCamera.style.display = "none";
			modalTabs.style.display = "none";
			modalCameraTech.style.display = "none";
			htmlStyle[0].style.overflow = "auto";
		}
	}, false);

	window.addEventListener('keydown', function(e){
		if(e.keyCode === 27){
			modalCamera.style.display = "none";
			modalTabs.style.display = "none";
			modalCameraTech.style.display = "none";
			htmlStyle[0].style.overflow = "auto";
		}
	}, false);


})();
