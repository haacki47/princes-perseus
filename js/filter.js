$(document).ready(function(){
	var myControllEl = $('#modalFullFilterBlock'),
			myModalFilter = $('#modalFullFilter');

	var blockFilterStyles = {
		minWidth: "160px",
		padding: "5px",
		backgroundColor: "#fff",
		border: "none",
		boxShadow: "0px 1px 2px rgba(0,0,0,.2)",
		position: "absolute",
		top:"20px",
		left: "-90px",
		zIndex: "9999",
	};

	var blockFilterCheckbox = {
		width: "100%",
		marginBottom: "10px",
		float: "none",
		marginRight: "none",
		color: "#000",
	};

	var inputGroupSelect = {
		marginTop: "10px",
		marginBottom: "10px",
		paddingRight: "30px",
	};

	if(screen.width <= 800){
		if(myControllEl && myModalFilter){

			$('.modal-body__info__date--time>button.btn').click(function(){
				myControllEl.css('display', 'none');
			});


		$('.input-group.date').css('marginTop', '10px');
		$('.input-group.select>select').css(inputGroupSelect);
		$('.modal-body__info__switcher.col-4').css(blockFilterCheckbox);
		$('#modalFullFilter>a.filterclick').click(function(){
		if(myControllEl.css('display') === "none"){
				myModalFilter.css('position', 'relative');
				myModalFilter.append(myControllEl);
				var myHiddenClass = myControllEl.children().find('hidden-sm');
				myHiddenClass.removeClass('hidden-sm');
				for(var x = 0; x < myHiddenClass.prevObject.length; x++){
					myHiddenClass.prevObject.removeClass('hidden-sm');
				}
				myControllEl.css('cssText', 'display: inline-block !important');
				myControllEl.css(blockFilterStyles);
			}else{
				myControllEl.css('cssText', 'display: none !important');
			}
		});
		
	}
	}
	

});