$(document).ready(function(){


	var openHeaderHelp = $('.modal-contact-valida'),
			buttonHeaderHelp = $('#ready-commit'),
			modalCameraTech = $('#modalCameraTech'),
			validaCloseBtn = $('.modal-contact-valida--close');
			var email = $('input[name="emphone"]').val();
			var name = $('input[name="name"').val();

		validaCloseBtn.click(function(){
			$('.modal-contact-valida').css('display', 'none');
		});


	modalCameraTech.submit(function(e){
		e.preventDefault();
		var inputReady = $('.form-group-modal>input');
		if(inputReady.hasClass('error')){
			return false;
		}else if(inputReady.hasClass('valid')){
			$.ajax({
                // url: "",
                method: 'POST',
                data: {
                	name: name,
                	emphone: email
                },
                success: function(data) {
                	$('#modalCameraTech').css('display', 'none');
                    openHeaderHelp.css('display', 'block');
                 },
                error: function(xhr, textStatus, errorObj) {
                   $('#modalCameraTech').css('display', 'none');
                    openHeaderHelp.css('display', 'block');
                 }
            });
		}
	});

	$(window).click(function(e){
		if(e.target === openHeaderHelp[0]){
			openHeaderHelp.css('display', 'none');
			$('#myhtml').css('overflow', 'auto');
		}
	});

});

