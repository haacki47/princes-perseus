/*
	**Dropdown menu and etc
	**By click to element we will have, open the full content
	**...
*/
;(function(){

	var doc = document,
			dropdown, btnAdd, openDropdown;


	btnAdd = doc.getElementsByClassName('dropbtn')[0];

	btnAdd.addEventListener('click', function(){
		doc.getElementById('dropdown-content').classList.toggle("show");
	}, false);

	// If user click missing this container
	window.addEventListener('click', function(e){
		if(!e.target.matches(".dropbtn")){
			dropdown = doc.getElementsByClassName('dropdown-content');
			for(var i = 0; i < dropdown.length; i++){
				openDropdown = dropdown[i];
				if(openDropdown.classList.contains("show")){
					openDropdown.classList.remove("show");
				}
			}
		}
	}, false);

})();